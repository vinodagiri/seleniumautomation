package framework.pk.pages;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import framework.pk.util.PropertyUtils;

public class BasePage {

	private static final Logger logger = LoggerFactory.getLogger(BasePage.class);
	private static final int EXPLICIT_TIMEOUT = Integer.parseInt("600");
	private static final int POLLING_TIME = Integer.parseInt("1000");
	protected  final String MENU = "//div[@class='menu']//b[contains(text(), '${VAL0}')]";
	protected  final String SUB_MENU = "//a[text() = '${VAL0}']";
	protected  final String DELETE_ROW = "//table[@id='resultTable']//tbody//a[text()='${VAL0}']/../../child::td//input[@type='checkbox']";

	private final WebDriver webDriver;
	private SearchContext searchContext;

	private JavascriptExecutor jsExecutor = (JavascriptExecutor) getDriver();
	private Select selectElement;
	private List<String> listDropdown = new ArrayList<>();
	protected EventFiringWebDriver eventDriver;
	protected WebDriverEventListener eventListener;

	public BasePage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	public PropertyUtils testProps() {
		return PropertyUtils.getInstance();
	}

	/**
	 * Enters value in a text field.
	 * 
	 * @return
	 */
	public void enterText(WebElement element, String text) {
		element.clear();
		element.sendKeys(text);
	}

	/**
	 * Get Text Of an Element
	 * 
	 * @param element
	 * @return
	 */
	protected String getText(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		return element.getText();
	}
	
	/**
	 * Get Text from an Attribute
	 * 
	 * @param element
	 * @return
	 */
	protected String getAttributeValue(WebElement element,String attributeValue) {
		waitFor(ExpectedConditions.visibilityOf(element));
		return element.getAttribute(attributeValue);
	}

	/**
	 * Gets the value of the given hidden element using JavaScript executor.
	 * 
	 * @param driver
	 * @param element
	 * 
	 * @return String
	 */
	protected String getTextFromHiddenElement(WebDriver driver, WebElement element) {
		return (String) jsExecutor.executeScript("return arguments[0].value;", element);
	}

	/**
	 * Select the checkbox element
	 * 
	 * @param element
	 */
	public void selectCheckbox(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		if (!element.isSelected()) {
			waitFor(ExpectedConditions.elementToBeClickable(element));
			clickElement(element);
		}
	}

	/**
	 * To Select the values from the drop down
	 * 
	 * @param topicListDropdown
	 * @return
	 */
	public List<String> getAllValuesInDropdown(WebElement dropDownElement) {
		selectElement = new Select(dropDownElement);
		List<WebElement> optionsList = selectElement.getOptions();
		for (WebElement optionList : optionsList) {
			listDropdown.add(optionList.getText());
		}
		return listDropdown;
	}

	/**
	 * Get selected text from the drop down
	 * 
	 * @param element
	 * @return
	 */
	public String getSelectedOption(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		selectElement = new Select(element);
		return selectElement.getFirstSelectedOption().getText();
	}

	/**
	 * Perform single click on a WebElement
	 * 
	 * @param element
	 */
	protected void clickElement(WebElement element) {
		moveToElement(element);
		element.click();
	}

	/**
	 * Perform double click on a WebElement using mouse actions
	 * 
	 * @param element
	 */
	public void doubleClick(WebElement element) {
		exec().moveToElement(element).doubleClick(element).build().perform();
	}

	/**
	 * Perform single click on a WebElement using mouse actions
	 * 
	 * @param element
	 */
	public void mouseClick(WebElement element) {
		exec().moveToElement(element).click().build().perform();
	}

	/**
	 * Perform click on a WebElement by matching text
	 * 
	 * @param elements
	 * @param link
	 */
	public void clickElementByText(List<WebElement> elements, String link) {
		for (WebElement webElement : elements) {
			if (link.equalsIgnoreCase(webElement.getText())) {
				clickElement(webElement);
				break;
			}
		}
	}


	/**
	 * Move to an element using Actions Class
	 * 
	 * @param element
	 */
	public void moveToElement(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		exec().moveToElement(element).build().perform();
	}

	/**
	 * Scroll into view to a WebElement
	 * 
	 * @param element
	 */
	public void scrollToElement(WebElement element) {
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	/**
	 * Check Element is displayed or not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementDisplayed(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		return element.isDisplayed();
	}

	/**
	 * Switch To Default Content
	 */
	public void switchToDefaultContent() {
		getDriver().switchTo().defaultContent();
	}

	/**
	 * Switches the context to a Frame [WebElement]
	 * 
	 * @param element
	 */
	public void switchToFrame(WebElement frameElement) {
		waitFor(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameElement));
	}

	/**
	 * Switch To Window
	 */
	public void switchToWindow() {
		checkPageIsReady();
		String currentWindow = getDriver().getWindowHandle();
		for (String winHandle : getDriver().getWindowHandles()) {
			if (!winHandle.equalsIgnoreCase(currentWindow)) {
				getDriver().switchTo().window(winHandle);
				break;
			}
		}
	}

	/**
	 * We are using this method to verifying the page load status
	 * 
	 */
	public void checkPageIsReady() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
				logger.error("caught exception while loading page{}", ex.getMessage());
				Assert.fail(ex.getMessage());
			}
			if ("complete".equalsIgnoreCase(((JavascriptExecutor) getDriver()).executeScript("return document.readyState").toString())) {
				break;
			}
		}
	}

	/**
	 * Confirm the presence of List of WebElements in a page.
	 * 
	 * @param List
	 *            of WebElements to be verified
	 * @return returns TRUE if all the elements are available
	 */
	public final boolean isElementsExist(final WebElement... allElements) {
		boolean isDisplayed = false;
		for (WebElement element : allElements) {
			try {
				if (element.isDisplayed()) {
					isDisplayed = true;
				}
			} catch (Exception e) {
				isDisplayed = false;
				logger.debug("Element not exist {}", element);
				break;
			}
		}
		return isDisplayed;
	}

	/**
	 * Confirm the unavailability of specified element.
	 * 
	 * @param Element
	 *            to be verified.
	 * @return It returns boolean value.
	 */
	public final boolean isElementNotExist(final WebElement element) {
		if (!isElementsExist(element)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the index of element whose text matches from the given text
	 *
	 * @param elements
	 * @return index of the element matching or -1 if no element found.
	 */
	public int getIndexOfElement(List<WebElement> elements, String text) {
		int index = 0;
		for (WebElement element : elements) {
			String elemText = element.getText();
			if (text.equals(elemText)) {
				break;
			}
			index++;
		}
		return index >= elements.size() ? -1 : index;
	}

	/**
	 * Return Elements Text [List Of Elements]
	 * 
	 * @param elements
	 * @return
	 */
	public List<String> getElementsByText(List<WebElement> elements) {
		waitFor(ExpectedConditions.visibilityOfAllElements(elements));
		List<String> listElements = new ArrayList<>();
		for (WebElement element : elements) {
			listElements.add(element.getText());
		}
		return listElements;
	}

	/**
	 * Does the javascript click on the page.
	 *
	 * @param element
	 * @return
	 */
	public BasePage jsClick(WebElement element) {
		jsExecutor.executeScript("arguments[0].click();", element);
		return this;
	}

	/**
	 * Returns actions class that will return the gestures that can be done on
	 * the web elements.
	 *
	 * @return
	 */
	protected Actions exec() {
		return new Actions(webDriver);
	}

	/**
	 * Performs mouse actions on the given element
	 *
	 * @param element
	 */
	protected void mouseOver(WebElement element) {
		exec().moveToElement(element).perform();
	}

	/**
	 * Get Alert Text
	 * 
	 * @return
	 */
	public String getAlertText() {
		return getDriver().switchTo().alert().getText();
	}

	/**
	 * Accept Alert
	 */
	public void acceptAlert() {
		getDriver().switchTo().alert().accept();
	}

	/**
	 * Dismiss Alert
	 */
	public void dismissAlert() {
		getDriver().switchTo().alert().dismiss();
	}

	/**
	 * Navigate Back
	 */
	public void navigateBack() {
		checkPageIsReady();
		getDriver().navigate().back();
	}

	/**
	 * Refresh A page
	 * 
	 * 
	 */
	public void refreshPage() {
		getDriver().navigate().refresh();
	}

	/**
	 * wait For Page Load Timeout
	 * 
	 * @param element
	 */
	public void waitForPageLoadTimeout(WebElement element) {
		getDriver().manage().timeouts().setScriptTimeout(180, TimeUnit.SECONDS);
		waitFor(ExpectedConditions.visibilityOf(element));
	}

	

	/**
	 * Wait for condition using the fluent driver
	 *
	 * @param waitCondition
	 */
	public <T> T waitFor(ExpectedCondition<T> waitCondition, long timeout) {
		return new WebDriverWait(webDriver, timeout, POLLING_TIME).until(waitCondition);
	}

	/**
	 * Waits for the specified condition until default timeout, ignoring
	 * exception
	 */
	public <T> T waitFor(ExpectedCondition<T> waitCondition, List<Class<? extends Exception>> exceptionsToIgnore,
			long timeout) {
		return new WebDriverWait(webDriver, timeout, POLLING_TIME).ignoreAll(exceptionsToIgnore).until(waitCondition);
	}

	/**
	 * Waits for the specified condition
	 */
	public <T> T waitFor(ExpectedCondition<T> waitCondition) {
		return new WebDriverWait(webDriver, EXPLICIT_TIMEOUT, POLLING_TIME)
				.ignoring(StaleElementReferenceException.class).until(waitCondition);
	}

	/**
	 * Waits for the specified condition until default timeout, ignoring
	 * exception
	 */
	public <T> T waitFor(ExpectedCondition<T> waitCondition, List<Class<? extends Exception>> exceptionsToIgnore) {
		return new WebDriverWait(webDriver, EXPLICIT_TIMEOUT, POLLING_TIME).ignoreAll(exceptionsToIgnore)
				.until(waitCondition);
	}

	/**
	 * Locates the given element using the by locator. Similar to @Findby and to
	 * be used in context where there are dynamic locators.
	 *
	 * @param locator
	 * @return
	 */
	public WebElement element(By locator) {
		return (WebElement) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] { WebElement.class },
				(object, method, args) -> {
					WebElement element = null;
					element = searchContext.findElement(locator);
					if (element != null) {
						return method.invoke(element, args);
					} else {
						return null;
					}
				});
	}

	/**
	 * Locates the given element using the by locator. Similar to @Findby and to
	 * be used in context where there are dynamic locators.
	 *
	 * @param locator
	 * @return
	 */
	public WebElement elementByWebDriver(By locator) {
		return (WebElement) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] { WebElement.class },
				(object, method, args) -> {
					WebElement element = getDriver().findElement(locator);
					return method.invoke(element, args);
				});
	}

	/**
	 * Locates the given element using the by locator. Similar to @Findby and to
	 * be used in context where there are dynamic locators.
	 *
	 * @param locator
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<WebElement> elements(By locator) {
		return (List<WebElement>) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] { List.class },
				(object, method, args) -> {
					List<WebElement> elements = searchContext.findElements(locator);
					return method.invoke(elements, args);
				});
	}

	/**
	 * Returns a dynamic web element whose locator
	 * 
	 * @param searchValue
	 * @return
	 */
	public String getDynamicXpathExpr(String xpathExpr, String... searchValues) {
		int count = 0;
		StringBuilder builder = new StringBuilder("${VAL}");
		for (String search : searchValues) {
			builder.replace(builder.length() - 1, builder.length(), count + "}");
			xpathExpr = xpathExpr.replace(builder.toString(), search);
			logger.debug("built xpath {} {} ", xpathExpr, search);
			builder.setLength(0);
			builder.append("${VAL}");
			count++;
		}
		logger.debug("Xpath expression {}", xpathExpr);
		return xpathExpr;
	}
	
	public void wait(int time) throws InterruptedException {
		Thread.sleep(time*1000);
	}

	/**
	 * Gets the URL for current Page.
	 * 
	 * @return String
	 */
	public String currentUrl() {
		return getDriver().getCurrentUrl();
	}

	/**
	 * Returns the title associated with web page.
	 *
	 * @return
	 */
	public String title() {
		return webDriver.getTitle();
	}

	/**
	 * @return the webDriver
	 */
	public WebDriver getDriver() {
		return webDriver;
	}

}
