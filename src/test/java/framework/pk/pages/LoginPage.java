package framework.pk.pages;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginPage extends WebPage {

	@FindBy(id = "txtUsername")
	private WebElement userName;
	
	@FindBy(id = "search_query_top")
	private WebElement searchQueryTxtBox;

	@FindBy(id = "txtPassword")
	private WebElement passWord;

	@FindBy(id = "btnLogin")
	private WebElement signIn;

	private WebDriver driver;

	private static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

	public LoginPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	/**
	 * Logs the user to the Application.
	 * 
	 * @return
	 */
	public LoginPage signIn(String userNm, String passWd) {
		try {
			waitFor(ExpectedConditions.visibilityOf(userName));
			enterText(userName, userNm);
			waitFor(ExpectedConditions.visibilityOf(passWord));
			enterText(passWord, passWd);
			clickButton("LOGIN");
			if(getDriver() instanceof  InternetExplorerDriver){
			}
		} catch (Exception ex) {
			logger.error("Exception occured on sign in ", ex);
		}
		return this;
	}

	/**
	 * Logs the user in to Automation Instance
	 */
	public LoginPage login() {
		logger.info("Launching the Automation Practice Application url ");
		driver.get("http://www.automationpractice.com/");
		waitFor(ExpectedConditions.visibilityOf(searchQueryTxtBox));
		return this;
	}

	public <T> T waitFor(ExpectedCondition<T> waitCondition) {
		return new WebDriverWait(driver, 600, 1000).ignoring(StaleElementReferenceException.class).until(waitCondition);
	}
}
