package framework.pk.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

public class SearchPage extends WebPage {
	
	@FindBy(id = "search_query_top")
	private WebElement searchQueryTxt;
	
	@FindBy(xpath = "//*[@id='index']/div[2]/ul/li")
	private List<WebElement> searchAutoResults;
	
	@FindBy(xpath = "//*[@id='center_column']//following-sibling::h1") //change
	private WebElement itemPropName;
	
	@FindBy(xpath = "//*[@id='center_column']/h1/span")
	private List<WebElement> searchProductListingResultsHeaders;
	
	@FindBy(xpath = "//*[@id='center_column']/ul/li")
	private List<WebElement> searchProductListingResults;
	
	@FindBy(xpath = "//fieldset[2]//following-sibling :: li")
	private List<WebElement> defaultcolorValue;
	
	@FindBy(xpath = "//a[@class='btn btn-default button-plus product_quantity_up']")
	private WebElement qtyPlusButton;
	
	@FindBy(xpath = "//a[@class='btn btn-default button-minus product_quantity_down']")
	private WebElement qtyMinusButton;
	
	@FindBy(xpath = "//*[@id='quantity_wanted_p']/input")
	private WebElement quantityButton;
	
	@FindBy(id = "our_price_display")
	private WebElement priceDisplay;
	
	@FindBy(xpath = "//*[@class='cart_block_list']//child::dt/span/a")
	private List<WebElement> removeProductsLink;
	
	
	@FindBy(xpath = "//*[@class='shopping_cart']/a")
	private WebElement cartList;
	
	@FindBy(xpath = "//*[contains(text(), 'Add to cart')]/..")
	private WebElement addToCartButton;
	
	@FindBy(xpath = "(//*[@id='layer_cart']//following-sibling::h2)[1]")
	private WebElement productSucessMessage;
	
	@FindBy(xpath = "//*[@id='layer_cart']//following-sibling::span[starts-with(@id,'layer_cart_product')]")
	private List<WebElement> productCartInfo;
	
	@FindBy(xpath = "(//*[@id='layer_cart']//following-sibling::a/..//span)[1]")
	private WebElement continueShoppingButton;
	
	@FindBy(xpath = "//*[@id='layer_cart']//ancestor::a")
	private WebElement proceedToCheckOutButton;
	
	@FindBy(id = "cart_title")
	private WebElement cartSummaryHeader;
	
	
	@FindBy(xpath = "//*[contains(@id,'product_price')]//span[@class='price']")
	private List<WebElement> unitPriceFromSummaryCart;
	
	@FindBy(xpath = "//*[contains(@id,'cart_quantity_up')]")
	private List<WebElement> quantityFromSummaryCart;
	
	@FindBy(xpath = "//*[contains(@class,'cart_quantity_delete')]")
	private List<WebElement> deleteButtonFromSummaryCart;
	
	@FindBy(xpath = "//td/span[text()='Total']/../..//td[2]/span")
	private WebElement totalPriceProducts;
	
	@FindBy(xpath = "//*[contains(@id,'total_product_price')]")
	private List<WebElement> totalQuantityValueFromSummaryCart;
	
	
	private String selectedValueListValue;
			
	
	private static final Logger logger = LoggerFactory.getLogger(SearchPage.class);

	public SearchPage(WebDriver driver) {
		super(driver);
	}
	
	public String getAttributeValueFromElement(){
		String searchPlaceHolderValue=getAttributeValue(searchQueryTxt,"placeholder");
		logger.info("search Place Holder Value is  :: "+searchPlaceHolderValue);
		return searchPlaceHolderValue;
	}	
	public boolean verifySearchAutoResultsPopulate(String searchQueryText) throws InterruptedException{
		boolean status=false;
		enterText(searchQueryTxt, searchQueryText);
		wait(2);
		waitFor(ExpectedConditions.visibilityOf(searchAutoResults.get(0)));
		logger.info("Search Auto Results Populate Size :: "+searchAutoResults.size());
		for(WebElement element : searchAutoResults) {
			String searchResultValue=element.getText();
		   if(searchResultValue.toLowerCase().contains(searchQueryText.toLowerCase())) {
			   element.click();
			   handleWebStaleException(itemPropName);
			   if(searchResultValue.toLowerCase().contains((itemPropName.getText()).toLowerCase())) {
				   status=true; 
				   break;
			   }
		   else { 
			   logger.info("Search text unavailable  :: "); }
		   }
	}
		return status;
}
	public boolean verifyProductResults(String searchQueryText){
		boolean resultsValue=false;
		enterText(searchQueryTxt, searchQueryText);
		clickButton("submit_search");
		waitFor(ExpectedConditions.visibilityOf(searchProductListingResultsHeaders.get(0)));
		logger.info("search Product Listing Results  Size :: "+searchProductListingResultsHeaders.size());
		 if((searchProductListingResultsHeaders.get(0).getText()).toLowerCase().contains(searchQueryText.toLowerCase())) {
			 resultsValue=true;
		 }
		return resultsValue;
		
	}
	
	public boolean verifyNumberOfProductList(){
		boolean resultsValue=true;
		try {
		String resultsPage=searchProductListingResultsHeaders.get(1).getText();
		int resultsNumber = Integer.parseInt(resultsPage.replaceAll("\\D", ""));
		for(int list=1;list<=resultsNumber;list++) {
			String productListStartXpath="//*[@id='center_column']/ul/li[";
			String productListEndXpath="]";
			logger.info("Search Products List are :: "+getDriver().findElement(By.xpath(productListStartXpath+list+productListEndXpath)).getText());
		  }
		}catch(Exception e) {
			logger.info("As per Results Products List are  not displaying");
			resultsValue=false;
		}
		return resultsValue;
	}
	
	
	public boolean verifyProductResultsPage(){
		boolean status = false;
		String currentColorSelectedValue;
		float priceValueAct;
		float priceValueEx = 0;
		String qtyValue;
		int rowCount=1; 
		logger.info("Search Auto Results Populate Size :: "+searchProductListingResults.size());

		int randomNumber=getRandomInteger(searchProductListingResults.size(),0);
		logger.info("random Number is :: "+randomNumber);
		selectProductFromResultList(randomNumber);
			
		  try {	
		     for (WebElement colorValue : defaultcolorValue){
			    String attributeColorValue=  colorValue.getAttribute("class");
			      if(!attributeColorValue.equals("selected") || defaultcolorValue.size()==1 ) {
			    	  colorValue.click();
			    	  waitFor(ExpectedConditions.elementToBeClickable(colorValue));
			    	  currentColorSelectedValue=getDriver().findElement(By.xpath("(//fieldset[2]//following-sibling :: li/a)["+rowCount+"]")).getAttribute("name");
			    	  logger.info("current Selected Value :: "+currentColorSelectedValue);
			    	  qtyPlusButton.click();
			    	  qtyValue= quantityButton.getAttribute("value");
			    	  logger.info("quantity Value selected :: "+qtyValue);
			    	  priceValueAct=Float.parseFloat(priceDisplay.getAttribute("innerHTML").replace("$", ""));
			    	  logger.info("price Value Act :: "+priceValueAct);
			    	 addToCartButton.click();
			    	 wait(2);
			    	 waitFor(ExpectedConditions.visibilityOf(productSucessMessage));
			    	 Assert.assertEquals(productSucessMessage.getText(), 
			    			 "Product successfully added to your shopping cart");
			    	 String cartProductName=productCartInfo.get(0).getText();
			    	  priceValueEx= Float.parseFloat(productCartInfo.get(3).getText().replace("$", ""));
			    	  
			    	 if(selectedValueListValue.toLowerCase().contains(cartProductName.toLowerCase()) && 
			    			 (productCartInfo.get(1).getText()).toLowerCase().contains(currentColorSelectedValue.toLowerCase()) &&
			    			 qtyValue.toLowerCase().contains( (productCartInfo.get(2).getText()).toLowerCase()) && 
			    			 priceValueEx == 2*priceValueAct  ) {
			    		 logger.info("Product Summary Page  displays as :: "+productCartInfo.get(0).getText()+
			    				 "Product color Value :: "+productCartInfo.get(1).getText()+
			    				 "Price value is "+ priceValueEx);
			    		 proceedToCheckOutButton.click();
			    		  status=verifyShoppingCartSummary(cartProductName,qtyValue);
			    		      break;
			    	 }else {
			    		 Assert.assertTrue(false, "Product summary is not matching as per user selected ");
			    	 }
			      
			      }
			      rowCount++;
		}
		}catch(Exception e) {
			e.printStackTrace();
			status=false;
		}
		return status;

}
	
	public boolean addMultipleProductsToCard(int selectProductFromList,int addMoreItems){
		boolean status = false;
		try {
			int resultsSize=searchProductListingResults.size();
			logger.info("add Multiple Products ToCard Size :: "+resultsSize);
			for(int productList=0;productList<=addMoreItems;productList++) {
				WebElement element=getDriver().findElement(By.xpath("(//*[@id='center_column']//following-sibling::h5/../div/a[1])["+(selectProductFromList)+"]"));
				logger.info("Clicking on add to cart  products :: "+selectProductFromList);
				 wait(2);
				((JavascriptExecutor)getDriver()).executeScript("arguments[0].click();",element );
				waitFor(ExpectedConditions.visibilityOf(productSucessMessage));
				Assert.assertEquals(productSucessMessage.getText(), 
						"Product successfully added to your shopping cart");
				continueShoppingButton.click();
				waitFor(ExpectedConditions.visibilityOf(searchProductListingResultsHeaders.get(0)));
				if(selectProductFromList == resultsSize) {
					selectProductFromList=selectProductFromList-2;}
				else {
					selectProductFromList++;
				}
				status = true;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			status=false;
		}
		return status;

	}
	
	
	public boolean verifyShoppingCartSummary(String productName,String quantityInDescrtionPage) {
		boolean summaryStatus=false;
		String summaryCartPage="SHOPPING-CART SUMMARY";
		waitFor(ExpectedConditions.visibilityOf(cartSummaryHeader));
		String xpathExpr = getDynamicXpathExpr(CART_SUMMARY, productName);
		waitFor(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(xpathExpr))));
		String quantityValueInSummaryCartPage=getDriver().findElement(By.xpath(xpathExpr)).getAttribute("value");
		if(quantityInDescrtionPage.equalsIgnoreCase(quantityValueInSummaryCartPage) &&
				cartSummaryHeader.getText().toLowerCase().contains(summaryCartPage.toLowerCase())){
			summaryStatus=true;
		}else {
			summaryStatus=false;
		}
		return summaryStatus;
	}
		
	
	public boolean verifyShoppingCartSummaryForMultipleAddedCarts() throws InterruptedException {
		boolean summaryStatus=false;
		((JavascriptExecutor)getDriver()).executeScript("arguments[0].click();",cartList );
		waitFor(ExpectedConditions.visibilityOf(cartSummaryHeader));
		quantityFromSummaryCart.get(1).click();
		 wait(2);
		deleteButtonFromSummaryCart.get(2).click();
		 wait(2);
		waitFor(ExpectedConditions.visibilityOf(totalPriceProducts));
		Float productPrice1=Float.parseFloat(totalQuantityValueFromSummaryCart.get(0).getAttribute("innerHTML").replace("$", ""));
		Float productPrice2=Float.parseFloat(totalQuantityValueFromSummaryCart.get(1).getAttribute("innerHTML").replace("$", ""));
		Float totalShipping=2.00f;
		float totalProductsPriceAfterUpdate=Float.parseFloat(totalPriceProducts.getText().replace("$", ""));
		if(totalProductsPriceAfterUpdate == productPrice1+productPrice2+totalShipping) {
			logger.info("total products price are matched with indiviual products :: "+totalProductsPriceAfterUpdate);
		summaryStatus=true;
				}
		else {
			Assert.assertTrue(false, "Quantity price are not Updated ");}
     
		return summaryStatus;
	}
	
	public void selectProductFromResultList(int selectProductFromList) {
//		logger.info("Search Auto Results Populate Size :: "+searchProductListingResults.size());
			WebElement element=getDriver().findElement(By.xpath("(//*[@id='center_column']//following-sibling::h5/a)["+(selectProductFromList+1)+"]"));
			selectedValueListValue=searchProductListingResults.get(selectProductFromList).getText();
			Actions builder = new Actions(getDriver());
			builder.moveToElement(searchProductListingResults.get(selectProductFromList)).perform();
			builder.doubleClick(element).perform();
			waitFor(ExpectedConditions.visibilityOf(itemPropName));
			String expectedSelectedValueInCart=itemPropName.getText().toLowerCase();
			if(selectedValueListValue.toLowerCase().contains(expectedSelectedValueInCart))
				 logger.info("The User Selected Product is  :: "+itemPropName.getText());
			else
				Assert.assertTrue(false, "Product is not displayed as per user selected ");
		
	}
	
	public void deleteProductsFromCartHomePage() {
		Actions builder = new Actions(getDriver());
		builder.moveToElement(cartList).click().perform();
		for(WebElement element : removeProductsLink) {
		builder.moveToElement(element).click().perform();
		}
	}
	public void handleWebStaleException(WebElement element) {
		for(int i=0;i<5;i++) {
		 try {
			   Thread.sleep(500);
			   break;
			   }catch(StaleElementReferenceException se){
				   logger.info("Stale Element Reference Exception :: " +i); 
				   se.getStackTrace();
			   }
		         catch(Exception se){
				   logger.info(" Exception in Product listing page :: " +i); 
				   se.getStackTrace();
			   }
		}
	}
	
	/* * returns random integer between minimum and maximum range */
	public static int getRandomInteger(int maximum, int minimum)
	{ 
		return ((int) (Math.random()*(maximum - minimum))) + minimum; 
	}

}