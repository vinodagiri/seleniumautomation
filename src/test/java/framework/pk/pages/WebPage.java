package framework.pk.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import framework.pk.report.StepReportData.StepStatus;
import framework.pk.testcases.BaseTest;

public class WebPage extends BasePage {

	protected static final String SUB_MENU = "//a[text() = '${VAL0}']";
	protected static final String CART_SUMMARY = "//table[@id='cart_summary']//tbody//tr//a[text()='${VAL0}']/../../../child::td//input[2]";
	private static final String BUTTON_INPUT = "//button[@type = 'button' or @type = 'submit'][contains(@value,'${VAL0}') or contains(@name,'${VAL0}')]";
	private static final Logger logger = LoggerFactory.getLogger(WebPage.class);
	
	

	public WebPage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(webDriver, this);
	}

	

	/**
	 * Clicks the button with the given name against a specific field
	 * 
	 * @param fieldlabel
	 * @param wmsButtonName
	 * @return
	 */
	public WebPage clickButton(String buttonName) {
		String xpathExpr = getDynamicXpathExpr(BUTTON_INPUT, buttonName);
		logger.info("click button xpath "+xpathExpr);
		WebElement element = waitFor(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(xpathExpr))));
		BaseTest.logStep(StepStatus.PASS, "", "Done", true);
		element.click();
		return this;
	}

	/**
	 * selects options for dropdown, multiselect By Visible Text.
	 * 
	 * @param element
	 * @param values
	 */
	public void selectByVisibleText(WebElement element, String... values) {
		if (values != null && values.length > 0) {
			Select select = new Select(element);
			for (String value : values) {
				select.selectByVisibleText(value);
			}
		}
	}
	
	/**
	 * Log step status
	 * 
	 * @param status
	 * @param stepDesc
	 * @param details
	 */
	/*public void logStep() {
		try {
			ITestResult result = Reporter.getCurrentTestResult();
			String methodName = result.getMethod().getMethodName();
			String scrnShotFileName = new StringBuilder(methodName).append("_step")
					.append(sequence.getAndIncrement()).append(".png").toString();
			ScreenshotUtil screenshotUtil = new ScreenshotUtil(getDriver(), BaseTest.screenShotDir);
			screenshotUtil.captureScreenShot(scrnShotFileName);
		} catch (Exception excp) {
			logger.error("Error while taking screenshots", excp);
		}
	}*/
}
