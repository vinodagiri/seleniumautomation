package framework.pk.report;

import com.relevantcodes.extentreports.LogStatus;

/**
 * Data holder for steps
 * @author avinod@orokarma.com
 * @version 1.0
 */
public class StepReportData {
	
	private final StepStatus status;
	private final String stepDesc;
	private final String details;
	private String screenShotLocation;
	private long startTime;
	private long endTime;
	
	/**
	 * Represents the step status
	 * 
	 * @author avinod@orokarma.com
	 *
	 */
	public enum StepStatus {
		PASS(LogStatus.PASS, "Passed"), FAIL(LogStatus.FAIL, "Failed"), SKIP(LogStatus.SKIP, "No Run");

		private final LogStatus status;
		private final String almStatus;

		private StepStatus(LogStatus status, String almStatus) {
			this.status = status;
			this.almStatus = almStatus;
		}

		/**
		 * @return the almStatus
		 */
		public String getAlmStatus() {
			return almStatus;
		}

		/**
		 * @return the status
		 */
		public LogStatus getStatus() {
			return status;
		}
	}
	
	/**
	 * Initialize step report.
	 * @param status
	 * @param stepDesc
	 * @param details
	 */
	public StepReportData(StepStatus status, String stepDesc, String details) {
		this.status = status;
		this.stepDesc = stepDesc;
		this.details = details;
		this.startTime = System.currentTimeMillis();
		this.endTime = startTime;
	}

	public StepReportData(StepStatus status, String stepDesc, String details, long startTime, long endTime) {
		this.status = status;
		this.stepDesc = stepDesc;
		this.details = details;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	/**
	 * @return the status
	 */
	public StepStatus getStatus() {
		return status;
	}

	/**
	 * @return the stepDesc
	 */
	public String getStepDesc() {
		return stepDesc;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @return the screenShotLocation
	 */
	public String getScreenShotLocation() {
		return screenShotLocation;
	}

	/**
	 * @param screenShotLocation the screenShotLocation to set
	 */
	public void setScreenShotLocation(String screenShotLocation) {
		this.screenShotLocation = screenShotLocation;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StepReportData [" + (status != null ? "status=" + status + ", " : "")
				+ (stepDesc != null ? "stepDesc=" + stepDesc + ", " : "")
				+ (details != null ? "details=" + details + ", " : "")
				+ (screenShotLocation != null ? "screenShotLocation=" + screenShotLocation : "") 
				+ "startTime=" + startTime 
				+ "endTime=" + endTime 
				+ "]";
	}
}
