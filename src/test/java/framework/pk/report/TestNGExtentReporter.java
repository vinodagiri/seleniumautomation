package framework.pk.report;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import framework.pk.report.StepReportData.StepStatus;

/**
 * Represents extent reporting to enable detailed graphical reports.
 * 
 * @author prokarma
 * @verion 1.0
 */
public class TestNGExtentReporter implements IReporter {
	private ExtentReports extent;

	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		extent = new ExtentReports(outputDirectory + File.separator + "Extent.html", true);
		extent.loadConfig(new File(System.getProperty("user.dir") + "/src/test/resources/extent-config.xml"));
		for (ISuite suite : suites) {
			Map<String, ISuiteResult> result = suite.getResults();

			for (ISuiteResult r : result.values()) {
				ITestContext context = r.getTestContext();
				buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
				buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
				buildTestNodes(context.getPassedTests(), LogStatus.PASS);
			}
			extent.flush();
		}
	}

	@SuppressWarnings("unchecked")
	private void buildTestNodes(IResultMap tests, LogStatus status) {
		ExtentTest test;

		if (tests != null && tests.size() > 0) {
			for (ITestResult result : tests.getAllResults()) {
				String testName = result.getMethod().getMethodName();
				Object[] values = result.getParameters();
				String finalValue = "";

				if (values.length != 0) {
					for (Object value : values) {
						finalValue = finalValue + value + " , ";
					}
					finalValue = finalValue.substring(0, finalValue.length() - 2);
				}
				test = extent.startTest(testName + " ( " + finalValue + " ) ");

				if (result.getMethod().getXmlTest().getAllParameters().get("platformName") != null) {
					test.assignCategory(result.getMethod().getXmlTest().getAllParameters().get("platformName"));
				}
				String message = "Test " + status.toString().toLowerCase() + "ed";

				if (result.getThrowable() != null) {
					message = result.getThrowable().getMessage();
				}
				test.setStartedTime(convertToDate(result.getStartMillis()).getTime());
				test.setEndedTime(convertToDate(result.getEndMillis()).getTime());
				List<StepReportData> reportData = (List<StepReportData>) result.getAttribute("Steps");
				if (reportData != null) {
					for (StepReportData data : reportData) {
						String imagePath = null;
						if (!StringUtils.isEmpty(data.getScreenShotLocation())) {
							imagePath = test.addScreenCapture(data.getScreenShotLocation());
						}
						if (data.getStatus() == StepStatus.FAIL && result.getThrowable() != null) {
							test.log(LogStatus.ERROR, data.getStepDesc(), imagePath + "<br/>" + result.getThrowable());
						} else {
							test.log(data.getStatus().getStatus(), data.getStepDesc(),
									imagePath + "<br/>" + data.getDetails());
						}

					}
				} else {
					test.log(status, message);
				}
				extent.endTest(test);
			}
		}

	}

	/*
	 * private List<String> readScreenShotNames(String methodName) {
	 * List<String> screenShots = new ArrayList<String>(); File dir = new
	 * File(BaseTest.screenShotDir.getAbsolutePath()); List<File> files =
	 * (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE,
	 * TrueFileFilter.INSTANCE); for (File file : files) { if
	 * (file.getName().contains(methodName)) { screenShots.add(file.getName());
	 * } } return screenShots; }
	 */

	private Calendar convertToDate(long time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal;
	}
}