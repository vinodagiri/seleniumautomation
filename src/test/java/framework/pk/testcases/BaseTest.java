package framework.pk.testcases;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.google.common.base.Optional;

import framework.pk.report.StepReportData.StepStatus;
import framework.pk.util.Constants;
import framework.pk.util.ExcelReader;
import framework.pk.util.PropertyUtils;
import framework.pk.util.TestNGStepLogger;

public class BaseTest {

	private static PropertyUtils testConfig;
	private static ExcelReader excelReader;
	private static final String EMPTY = "";
	public static final String PLATFORM_NAME = "platformName";
	private WebDriver primaryDriver;
	public static File screenShotDir;
	private static final Logger logger = LoggerFactory.getLogger(BaseTest.class);
	private static final String WEB_DRIVER_ATTR = "web.driver";
	
	@BeforeSuite
	public void initialize() {
		try {
			testConfig = PropertyUtils.getInstance();
			excelReader = new ExcelReader(testConfig.getProperty("excelfile"));
			excelReader.prepareRowMap(testConfig.getProperty("sheetname"), "Test Name");
			screenShotDir=new File(testConfig.getProperty("screenshotdir"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	@BeforeMethod
	public void beforeMethod(ITestContext testContext, ITestResult result) throws MalformedURLException {
		Map<String, String> testParameters = null;
		testParameters = testContext.getCurrentXmlTest().getLocalParameters();
		String platformName = Optional.fromNullable(testParameters.get(PLATFORM_NAME)).or(EMPTY);
		DesiredCapabilities theDesiredCapabilities = null;

		// Get hub URL from test property file
		String hubURL = testConfig.getProperty("hubURL");
		primaryDriver = null;
		if (primaryDriver == null) {
			if (platformName.equalsIgnoreCase("chrome")) {
				theDesiredCapabilities = getChromeCapabilities(testConfig.getProperty(Constants.CHROME_DRIVER_PATH));
			} else if (platformName.equalsIgnoreCase("firefox")) {
				theDesiredCapabilities = getFirefoxCapabilities(testConfig.getProperty(Constants.FIREFOX_DRIVER_PATH));
			} else if (platformName.equalsIgnoreCase("ie")) {
				theDesiredCapabilities = getIECapabilities(testConfig.getProperty(Constants.IE_DRIVER_PATH));
			}

			if (testConfig.getProperty("runMode").equalsIgnoreCase("local")) {
				if (platformName.equalsIgnoreCase("firefox")) {
					primaryDriver = new FirefoxDriver(theDesiredCapabilities);
				} else if (platformName.equalsIgnoreCase("chrome")) {
					primaryDriver = new ChromeDriver(theDesiredCapabilities);
				} else if (platformName.equalsIgnoreCase("ie")) {
					primaryDriver = new InternetExplorerDriver(theDesiredCapabilities);
				}
			} else {
				primaryDriver = new RemoteWebDriver(new URL(hubURL), theDesiredCapabilities);
			}
		}
		result.setAttribute(WEB_DRIVER_ATTR, primaryDriver);
		primaryDriver.manage().window().maximize();
		primaryDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	private DesiredCapabilities getChromeCapabilities(String driverPath) throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", driverPath);
		DesiredCapabilities theDesCap = DesiredCapabilities.chrome();
		((DesiredCapabilities) theDesCap).setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		theDesCap.setBrowserName("chrome");
		return theDesCap;
	}

	private DesiredCapabilities getFirefoxCapabilities(String driverPath) {
			System.setProperty("webdriver.gecko.driver", driverPath);
		DesiredCapabilities theDesCap = DesiredCapabilities.firefox();
		((DesiredCapabilities) theDesCap).setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

		return theDesCap;
	}

	private DesiredCapabilities getIECapabilities(String driverPath) throws MalformedURLException {
		System.setProperty("webdriver.ie.driver", driverPath);
		DesiredCapabilities theDesCap = DesiredCapabilities.internetExplorer();
		theDesCap.setCapability("requireWindowFocus", true);
		theDesCap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		theDesCap.setCapability("disable-popup-blocking", true);
		theDesCap.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, UnexpectedAlertBehaviour.IGNORE);
		theDesCap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		return theDesCap;
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult result, Method testMethod, Object[] testData) {
		WebDriver driver = null;
		try {
			driver = getDriverFromResult(result);
			logger.debug("Web driver is {} {}", getDriver(), Thread.currentThread().getId());
			if (driver != null) {
				if ((ITestResult.FAILURE == result.getStatus()
						|| (result.getThrowable() != null && !(result.getThrowable() instanceof SkipException)))) {
					TestNGStepLogger.logStep(StepStatus.FAIL, "Last Step Failed For Test", result.getThrowable().getMessage().toString(), driver, result, true);
				} else if ((ITestResult.SUCCESS == result.getStatus()
						|| ITestResult.SUCCESS_PERCENTAGE_FAILURE == result.getStatus())) {
					TestNGStepLogger.logStep(StepStatus.PASS, "Successfully Competed Test", "Done", driver, result, true);
				}
			}
		} catch (Throwable thr) {
			logger.error("Error while ending the test {}", driver, thr);
		} finally {
			if (driver != null) {
				closeBrowser(result);
			}
		}
	}

	/**
	 * Closes the given browser
	 */
	public void closeBrowser(ITestResult result) {
		try {
			WebDriver driver = getDriverFromResult(result);
			if (driver != null) {
				driver.quit();
			}
		} catch (Exception excp) {
			logger.error("Error while closing browser", excp);
		} finally {
			result.removeAttribute(WEB_DRIVER_ATTR);
			logger.debug("Web driver is {} {}", getDriverFromResult(result), Thread.currentThread().getId());
		}
	}

	/**
	 * Returns an instance of the web driver.
	 * 
	 * @return
	 */
	public WebDriver getDriver() {
		return getDriverFromResult(Reporter.getCurrentTestResult());
	}
	
	private static WebDriver getDriverFromResult(ITestResult result) {
		return (WebDriver) result.getAttribute("web.driver");
	}

	
	/**
	 * Logs step status
	 * 
	 * @param status
	 * @param stepDesc
	 * @param details
	 */
	public static void logStep(StepStatus status, String stepDesc, String details, boolean captureScreenShot) {
		ITestResult result = Reporter.getCurrentTestResult();
		TestNGStepLogger.logStep(status, stepDesc, details, getDriverFromResult(result), result, captureScreenShot);
	}
	
	/**
	 * Returns Excel Reader
	 * 
	 * @return
	 */
	protected ExcelReader getExcelReader() {
		return excelReader;
	}

	/**
	 * Captures the screen shot.
	 * 
	 * @param status
	 * @param stepDesc
	 * @param details
	 */
	/*public void logStep() {
		try {
			ITestResult result = Reporter.getCurrentTestResult();
			String methodName = result.getMethod().getMethodName();
			String scrnShotFileName = new StringBuilder(methodName).append("_step").append(sequence.getAndIncrement())
					.append(".png").toString();
			ScreenshotUtil screenshotUtil = new ScreenshotUtil(getDriver(), screenShotDir);
			screenshotUtil.captureScreenShot(scrnShotFileName);
		} catch (Exception excp) {
			logger.error("Error while taking screenshots", excp);
		}
	}*/

}
