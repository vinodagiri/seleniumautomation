
package framework.pk.testcases;

import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.Test;
import framework.pk.pages.LoginPage;
import framework.pk.pages.SearchPage;

public class SearchTest extends BaseTest {
	@Test
	public void validateSearchFuntionalityWithAutoSuggestionLinks() throws InterruptedException {
		final Map<String, String> testData = getExcelReader().getRowFromMap("validateSearchFuntionalityWithAutoSuggestionLinks");
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.login();
		SearchPage searchPage = new SearchPage(getDriver());
		String searchPlaceHolderValueActual=searchPage.getAttributeValueFromElement();
		String searchPlaceHolderValueExpected=testData.get("ExpectedValue");
		Assert.assertEquals(searchPlaceHolderValueActual, searchPlaceHolderValueExpected,"Both values are not equal");
		boolean searchPageValue=searchPage.verifySearchAutoResultsPopulate(testData.get("SearchQuery"));
		Assert.assertTrue(searchPageValue, "application unable to populate with given search value");
	}
	
	@Test
	public void validateSummaryCartOnSingleProductSelection() {
		final Map<String, String> testData = getExcelReader().getRowFromMap("validateSummaryCartOnSingleProductSelection");
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.login();
		SearchPage searchPage = new SearchPage(getDriver());
		boolean resultsPageValue=searchPage.verifyProductResults(testData.get("SearchQuery"));
		Assert.assertTrue(resultsPageValue, "Product listing is not displayed as per user value");
		boolean productResultsListValues=searchPage.verifyNumberOfProductList();
		Assert.assertTrue(productResultsListValues, "Product Resulst list pages are not displayed as per user value");
		boolean productResultsPages=searchPage.verifyProductResultsPage();
		Assert.assertTrue(productResultsPages, "Product in results page are matching");
		
	}
	
	@Test
	public void validateSummaryCartOnMultipleProductSelection() throws InterruptedException {
		final Map<String, String> testData = getExcelReader().getRowFromMap("validateSummaryCartOnMultipleProductSelection");
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.login();
		SearchPage searchPage = new SearchPage(getDriver());
		boolean resultsPageValue=searchPage.verifyProductResults(testData.get("SearchQuery"));
		Assert.assertTrue(resultsPageValue, "Product listing is not displayed as per user value");
		boolean productResultsListValues=searchPage.addMultipleProductsToCard(3,2);
		Assert.assertTrue(productResultsListValues, "Mulitple Products are not displayed Resulst in list pages areas per user value");
		boolean productResultsPages=searchPage.verifyShoppingCartSummaryForMultipleAddedCarts();
		Assert.assertTrue(productResultsPages, "Total Price and indivual products price are not matching");
		
	}	
}
