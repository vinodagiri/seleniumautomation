package framework.pk.util;

public class Constants {
	public static final String URL = "url";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String CHROME_DRIVER_PATH = "chromeDriverPath";
	public static final String FIREFOX_DRIVER_PATH = "firefoxDriverPath";
	public static final String IE_DRIVER_PATH = "ieDriverPath";

}
