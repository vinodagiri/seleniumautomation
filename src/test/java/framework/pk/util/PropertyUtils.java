package framework.pk.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyUtils {

	private static final Logger logger = LoggerFactory.getLogger(PropertyUtils.class);
	private static PropertyUtils propertyUtils;
	private final static Properties myConfigProperties = new Properties();

	private PropertyUtils() {
		InputStream systemResource = ClassLoader.getSystemResourceAsStream("run.properties");
		try {
			myConfigProperties.load(systemResource);
		} catch (FileNotFoundException fex) {
			logger.error("FileNotFoundException {}", fex.getMessage());
		} catch (IOException ioe) {
			logger.error("IOException {}", ioe.getMessage());
		}
	}

	public String getProperty(final String key) {
		return myConfigProperties.getProperty(key);
	}

	public Set<String> getAllPropertyNames() {
		return myConfigProperties.stringPropertyNames();
	}

	public boolean containsKey(final String key) {
		return myConfigProperties.containsKey(key);
	}

	public static PropertyUtils getInstance() {
		if (propertyUtils == null) {
			propertyUtils = new PropertyUtils();
		}
		return propertyUtils;
	}
}
