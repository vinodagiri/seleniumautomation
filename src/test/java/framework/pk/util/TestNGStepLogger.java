package framework.pk.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;

import framework.pk.report.StepReportData;
import framework.pk.report.StepReportData.StepStatus;
import framework.pk.testcases.BaseTest;

/**
 * A utility class that will help logging of test steps from different location.
 * 
 * @author Vinod
 * @version 1.0
 *
 */
public class TestNGStepLogger {

	private static final String STEPS = "Steps";
	private static final AtomicLong sequence = new AtomicLong(1);
	private static final Logger logger = LoggerFactory.getLogger(TestNGStepLogger.class);

	private TestNGStepLogger() {
	}

	@SuppressWarnings({ "unchecked" })
	private static void commonLogStep(StepStatus status, String stepDesc, String details, ITestResult result, String addInfo) {
		List<StepReportData> data = (List<StepReportData>) result.getAttribute(STEPS);
		logger.debug("Data {} {} {} {} {}", result.getInstanceName(), result.getAttributeNames(), result.getName(),
				data, new Exception().getStackTrace()[1] + addInfo);
		if (data == null) {
			data = new ArrayList<StepReportData>();
			result.setAttribute(STEPS, data);
		}
		data.add(new StepReportData(status, stepDesc, details));
		logger.debug("Data {} {} {} {} {}", result.getInstanceName(), result.getAttributeNames(), result.getName(),
				data, new Exception().getStackTrace()[1]);
	}

	@SuppressWarnings("unchecked")
	public static void logStep(StepStatus status, String stepDesc, String details, WebDriver driver, ITestResult result, boolean captureScreenShot) {
		try {
			commonLogStep(status, stepDesc, details, result, "");
			String methodName = result.getMethod().getMethodName();
			List<StepReportData> data = (List<StepReportData>) result.getAttribute(STEPS);
			logger.debug("Data {} {} {} {} {}", result.getInstanceName(), result.getAttributeNames(), result.getName(),
					data, new Exception().getStackTrace()[2]);
			if (driver != null) {
				int count = data.size();
				String scrnShotFileName = new StringBuilder(methodName).append("_step").append(count).append("_seq")
						.append(sequence.getAndIncrement()).append(".png").toString();
				ScreenshotUtil screenshotUtil = new ScreenshotUtil(driver, BaseTest.screenShotDir);
				screenshotUtil.captureScreenShot(scrnShotFileName);
				data.get(count - 1).setScreenShotLocation(new StringBuilder(BaseTest.screenShotDir.getAbsolutePath())
						.append(File.separator).append(scrnShotFileName).toString());
			}
		} catch (Exception excp) {
			logger.error("Error while taking screenshots", excp);
		}
	}
}